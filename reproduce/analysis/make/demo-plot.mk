# Second step of analysis:
#    Data for plot of number/fraction of tools per year.
#
# Copyright (C) 2020-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Table for Figure 1C of Menke+20
# -------------------------------
a2mk20f1c = $(tex-publish-dir)/tools-per-year.txt
$(a2mk20f1c): $(mk20tab3) | $(tex-publish-dir)

        # Remove the (possibly) produced figure that is created from this
        # table: it is created by LaTeX's TiKZ package, and includes
        # multiple files with a fixed prefix.
	rm -f $(tikzdir)/figure-tools-per-year*

        # Write the column metadata in a temporary file name (appending
        # '.tmp' to the actual target name). Once all steps are done, it is
        # renamed to the final target. We do this because if there is an
        # error in the middle, Make will not consider the job to be
        # complete and will stop here.
	echo "# Data of plot showing fraction of papers that mentioned software tools" > $@.tmp
	echo "# per year to demonstrate the features of Maneage (MANaging data linEAGE)." >> $@.tmp
	     >> $@.tmp
	echo "# Raw data taken from Menke+2020 (https://doi.org/10.1101/2020.01.15.908111)." \
	     >> $@.tmp
	echo "# " >> $@.tmp
	echo "# Column 1: YEAR       [count, u16] Publication year of papers." \
	     >> $@.tmp
	echo "# Column 2: WITH_TOOLS [frac,  f32] Fraction of papers mentioning software tools." \
	     >> $@.tmp
	echo "# Column 3: NUM_PAPERS [count, u32] Total number of papers studied in that year." \
	     >> $@.tmp
	echo "# " >> $@.tmp
	$(call print-general-metadata, $@.tmp)


        # Find the maximum number of papers.
	awk '!/^#/{all[$$1]+=$$2; id[$$1]+=$$3} \
	     END{ for(year in all) \
	            printf("%-7d%-10.3f%d\n", year, 100*id[year]/all[year], \
	                   all[year]) \
	        }' $< \
	    >> $@.tmp

        # Write it into the final target
	mv $@.tmp $@





# Final LaTeX macro
$(mtexdir)/demo-plot.tex: $(a2mk20f1c) $(pconfdir)/demo-year.conf

        # Find the first year (first column of first row) of data.
	v=$$(awk '!/^#/ && c==0{c++; print $$1}' $(a2mk20f1c))
	echo "\newcommand{\menkefirstyear}{$$v}" > $@

        # Find the number of rows in the plotted table.
	v=$$(awk '!/^#/{c++} END{print c}' $(a2mk20f1c))
	echo "\newcommand{\menkenumyears}{$$v}" >> $@

        # Find the number of papers in 1996.
	v=$$(awk '$$1==$(menke-demo-year){print $$3}' $(a2mk20f1c))
	echo "\newcommand{\menkenumpapersdemocount}{$$v}" >> $@
	echo "\newcommand{\menkenumpapersdemoyear}{$(menke-demo-year)}" >> $@
