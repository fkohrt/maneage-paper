# First step of analysis:
#    Prepare the data, return basic values.
#
# As a demonstration analysis to go with the paper, we use the data from
# Menke 2020 (DOI:10.1101/2020.01.15.908111). This is a relevant paper
# because it provides interesting statistics about tools and methods used
# in scientific papers.
#
# Copyright (C) 2020-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Save the "Table 3" spreadsheet from the downloaded `.xlsx' file into a
# simple plain-text file that is easy to use.
a1dir = $(badir)/analysis1
mk20tab3 = $(a1dir)/table-3.txt
$(a1dir):; mkdir $@
$(mk20tab3): $(indir)/menke20.xlsx | $(a1dir)

        # Set a base-name for the table-3 data.
	base=$(basename $(notdir $<))-table-3

        # Unfortunately XLSX I/O only works when the input and output are
        # in the directory it is running. So first, we need to switch to
        # the input directory, run it, then put our desired output where we
        # want and delete the extra files.
	topdir=$$(pwd)
	cd $(indir)
	xlsxio_xlsx2csv $(notdir $<)
	cp $(notdir $<)."Table 3 All by journal by year".csv $$base.csv
	rm $(notdir $<).*.csv
	cd $$topdir

        # Read the necessary information. Note that we are dealing with a
        # CSV (comma-separated value) file. But when there are commas in a
        # string, quotation signs are put around it. The `FPAT' values is
        # fully described in the GNU AWK manual. In short, it ensures that
        # if there is a comma in the middle of double-quotes, it doesn't
        # count as a delimter.
	echo "# Column 1: YEAR [counter, i16] Year of journal's publication." > $@.tmp
	echo "# Column 2: NUM_PAPERS [counter, i16] Number of studied papers in that journal." >> $@.tmp
	echo "# Column 3: NUM_PAPERS_WITH_TOOLS [counter, i16] Number of papers with an identified tool." >> $@.tmp
	echo "# Column 4: NUM_ID_TOOLS [counter, i16] Number of software/tools that were identified." >> $@.tmp
	echo "# Column 5: JOURNAL_NAME [string, str150] Name of journal." >> $@.tmp
	awk 'NR>1{printf("%-10d%-10d%-10d%-10d %s\n", $$2, $$3, $$3*$$NF, $$(NF-1), $$1)}' \
	    FPAT='([^,]+)|("[^"]+")' $(indir)/$$base.csv >> $@.tmp

        # Set the temporary file as the final target. This was done so if
        # there is any possible crash in the steps above, this rule is
        # re-run (its final target isn't rebuilt).
	mv $@.tmp $@





# Main LaTeX macro file
$(mtexdir)/format.tex: $(mk20tab3)

        # Count the total number of papers in their study.
	v=$$(awk '!/^#/{c+=$$2} END{print c}' $(mk20tab3))
	echo "\newcommand{\menkenumpapers}{$$v}" > $@

        # Count how many unique journals there were in the study. Note that
        # the `31' comes because we put 10 characters for each numeric
        # column and separated the last numeric column from the string
        # column with a space. If the number of numeric columns change in
        # the future, the `31' also has to change.
	v=$$(awk 'BEGIN{FIELDWIDTHS="41 10000"} !/^#/{print $$2}' \
	         $(mk20tab3) | uniq | wc -l)
	echo "\newcommand{\menkenumjournals}{$$v}" >> $@

        # Count how many rows the original catalog has.
	v=$$(awk '!/^#/{c++} END{print c}' $(mk20tab3))
	echo "\newcommand{\menkenumorigrows}{$$v}" >> $@
